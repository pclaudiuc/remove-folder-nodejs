import * as util from "util";
import * as fs from "fs"


async function removeFolder(folderToRemove:string) {
    
    let rmdir = util.promisify(fs.rmdir);
    let readdir = util.promisify(fs.readdir);
    let unlink = util.promisify(fs.unlink);
    let stack: string[] = [];
    stack.push(folderToRemove);
    while (stack.length > 0) {
        let currentFolder = stack[stack.length - 1]
        let content = await readdir(currentFolder, { withFileTypes: true })
            .catch(error => {
                console.error(`Error reading content! ${currentFolder} ` + error)

                throw new Error("Error reading content!")
            });
        if (content.length > 0) {//folder is not empty
            let folders = content.filter(stat => {
                if (stat.isDirectory())
                    return stat
            })
            if (folders.length > 0) {//there are subfolders
                folders.forEach(folder => stack.push(currentFolder + "/" + folder.name))
            }
            else {//just files 
                let tempPromises: Promise<void>[] = [];
                await Promise.all(content.map(file => unlink(currentFolder + "/" + file.name)))
                    .catch(error => {
                        console.error(`Error deleting file! ${currentFolder}` + error)

                        throw new Error("Error deleting file!")
                    });

                //now folder is empty 
                await rmdir(currentFolder)
                    .catch(error => {
                        console.error(`Error deleting folder () ${currentFolder}` + error)

                        throw new Error("Error deleting folder")
                    })
                stack.pop();
            }
        }
        else {//folder is empty
            await rmdir(currentFolder)
                .catch(error => {
                    console.error(`Error deleting folder  ${currentFolder}` + error)

                    throw new Error("Error deleting folder")
                })
            stack.pop();
        }
    }
}
var folderToRemove = process.argv[2];

console.log("deleting....")
removeFolder(folderToRemove)
    .then(ok => console.log("OK"))
    .catch(error => console.log(error))

